#include <dlfcn.h>

#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"

FelixClientThread::FelixClientThread(const Config& config) {
    // load shared library that implements the API from the FELIX release
    handler = dlopen("libfelix-client-lib.so", RTLD_NOW);
    if (!handler) {
        throw std::runtime_error(dlerror());
    }

    // reset error
    dlerror();

    // get a pointer to the function that
    // will create a new instance of the FelixClientThread implementation
    creator = reinterpret_cast<ImplCreator>(dlsym(handler, "create_felix_client"));
    const char * dlsym_error = dlerror();
    if (dlsym_error) {
        throw std::runtime_error(dlsym_error);
    }

    implementation = std::unique_ptr<FelixClientThreadInterface>(creator(config));
    extension = dynamic_cast<FelixClientThreadExtension*>(implementation.get());
    extension42 = dynamic_cast<FelixClientThreadExtension42*>(implementation.get());
    extension421 = dynamic_cast<FelixClientThreadExtension421*>(implementation.get());
    extension422 = dynamic_cast<FelixClientThreadExtension422*>(implementation.get());
    extension423 = dynamic_cast<FelixClientThreadExtension423*>(implementation.get());
}

FelixClientThread::~FelixClientThread() {
    if (handler) {
        dlclose(handler);
    }
}

// interface
void FelixClientThread::send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush) {
    implementation->send_data(fid, data, size, flush);
}

void FelixClientThread::send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes){
    if (extension421) {
       extension421->send_data(fid, msgs, sizes);
    } else {
        throw FelixClientException("send_data (vector) not implemented");
    }
}


void FelixClientThread::send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) {
    if (extension422) {
       extension422->send_data_nb(fid, data, size, flush);
    } else {
        throw FelixClientException("non-blocking send_data not implemented");
    }
}

void FelixClientThread::send_data_nb(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes){
    if (extension422) {
       extension422->send_data_nb(fid, msgs, sizes);
    } else {
        throw FelixClientException("non-blocking send_data (vector) not implemented");
    }
}


void FelixClientThread::subscribe(uint64_t fid) {
    implementation->subscribe(fid);
}

void FelixClientThread::unsubscribe(uint64_t fid) {
    implementation->unsubscribe(fid);
}

void FelixClientThread::exec(const UserFunction &user_function) {
    implementation->exec(user_function);
}

// extension
void FelixClientThread::init_send_data(uint64_t fid) {
    if (extension) {
        extension->init_send_data(fid);
    } else {
        throw FelixClientException("init_send_data not implemented");
    }
}

void FelixClientThread::init_subscribe(uint64_t fid) {
    if (extension) {
        extension->init_subscribe(fid);
    } else {
        throw FelixClientException("init_subscribe not implemented");
    }
}

FelixClientThread::Status FelixClientThread::send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) {
    if (extension42) {
        return extension42->send_cmd(fids, cmd, cmd_args, replies);
    } else {
        throw FelixClientException("send_cmd not implemented");
    }
}

void FelixClientThread::subscribe(const std::vector<uint64_t>& fids) {
    if (extension42) {
        extension42->subscribe(fids);
    } else {
        throw FelixClientException("subcribe(vector) not implemented");
    }
}

void FelixClientThread::user_timer_start(unsigned long interval) {
    if (extension423) {
        extension423->user_timer_start(interval);
    } else {
        throw FelixClientException("user_timer_start(interval) not implemented");
    }
}

void FelixClientThread::user_timer_stop() {
    if (extension423) {
        extension423->user_timer_stop();
    } else {
        throw FelixClientException("user_timer_stop() not implemented");
    }
}

void FelixClientThread::callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) {
    if (extension423) {
        extension423->callback_on_user_timer(on_user_timer_cb);
    } else {
        throw FelixClientException("callback_on_user_timer(on_user_timer_cb) not implemented");
    }
}

// if in extension
// void FelixClientThread::init_subscribe(uint64_t fid) {
//     if (extension) {
//         extension->init_subscribe(fid);
//     } else {
//         throw FelixClientException("init_subscribe not implemented");
//     }
// }
