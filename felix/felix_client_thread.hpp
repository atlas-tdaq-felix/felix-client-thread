#pragma once

#include <memory>
#include <sstream>
#include <vector>

#include "felix/felix_client_thread_extension.hpp"
#include "felix/felix_client_thread_extension42.hpp"
#include "felix/felix_client_thread_extension421.hpp"
#include "felix/felix_client_thread_extension422.hpp"
#include "felix/felix_client_thread_extension423.hpp"

class FelixClientThread : public FelixClientThreadExtension423 {

public:
    explicit FelixClientThread(const Config& config);
    ~FelixClientThread();

    void send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
    void send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;

    void send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
    void send_data_nb(uint64_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;

    void subscribe(uint64_t fid) override;
    void subscribe(const std::vector<uint64_t>& fids) override;
    void unsubscribe(uint64_t fid) override;

    void exec(const UserFunction &user_function) override;

    void init_send_data(uint64_t fid) override;
    void init_subscribe(uint64_t fid) override;

    Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) override;

    void user_timer_start(unsigned long interval) override;
    void user_timer_stop() override;
    void callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) override;

    // Not yet in interface

    // Only for FELIX internal
    enum Key { UNKNOWN = 0, UUID = 1, CMD = 2, CMD_ARGS = 3, VALUE = 4, STATUS = 5, MESSAGE = 6 };

    inline static std::string to_string(Key key) {
        switch(key) {
            case UUID:
                return "uuid";
            case CMD:
                return "cmd";
            case CMD_ARGS:
                return "cmd_args";
            case VALUE:
                return "value";
            case STATUS:
                return "status";
            case MESSAGE:
                return "message";
            default:
                return "unknown";
        }
    }

    inline static Key to_key(const std::string& key) {
        if (key == "uuid") return UUID;
        if (key == "cmd") return CMD;
        if (key == "cmd_args") return CMD_ARGS;
        if (key == "value") return VALUE;
        if (key == "status") return STATUS;
        if (key == "message") return MESSAGE;
        return Key::UNKNOWN;
    }

    inline static std::string to_string(Cmd cmd) {
        switch(cmd) {
            case NOOP:
                return "noop";
            case GET:
                return "get";
            case SET:
                return "set";
            case TRIGGER:
                return "trigger";
            case ECR_RESET:
                return "ecr_reset";
            default:
                return "unknown";
        }
    }

    inline static FelixClientThreadExtension::Cmd to_cmd(const std::string& cmd) {
        if (cmd == "noop") return FelixClientThreadExtension::Cmd::NOOP;
        if (cmd == "get") return FelixClientThreadExtension::Cmd::GET;
        if (cmd == "set") return FelixClientThreadExtension::Cmd::SET;
        if (cmd == "trigger") return FelixClientThreadExtension::Cmd::TRIGGER;
        if (cmd == "ecr_reset") return FelixClientThreadExtension::Cmd::ECR_RESET;
        return FelixClientThreadExtension::Cmd::UNKNOWN;
    }

    inline static std::string to_string(Status status) {
        switch(status) {
            case OK:
                return "Ok";
            case ERROR:
                return "Error";
            case ERROR_NO_SUBSCRIPTION:
                return "Error: could not subscribe for replies";
            case ERROR_NO_CONNECTION:
                return "Error: could not send command";
            case ERROR_NO_REPLY:
                return "Error: received no reply for command";
            case ERROR_INVALID_CMD:
                return "Error: invalid command";
            case ERROR_INVALID_ARGS:
                return "Error: invalid (number of) arguments";
            case ERROR_INVALID_REGISTER:
                return "Error: invalid register name";
            case ERROR_NOT_AUTHORIZED:
                return "Error: not authorized";

            default:
                return "Unknown";
        }
    }

    inline static std::string to_string(const Reply& reply) {
        std::ostringstream stream;
        stream << "Reply: {ctrl_fid: 0x" << std::hex << reply.ctrl_fid << ", status: " << std::dec << reply.status << ", message: \"" << reply.message << "\", value: 0x" << std::hex << reply.value << "}" << std::endl;
        return stream.str();
    }

private:
    typedef FelixClientThreadInterface * (*ImplCreator)(const Config& config);

    void * handler = nullptr;
    ImplCreator creator = nullptr;
    std::unique_ptr<FelixClientThreadInterface> implementation;
    FelixClientThreadExtension* extension;
    FelixClientThreadExtension42* extension42;
    FelixClientThreadExtension421* extension421;
    FelixClientThreadExtension422* extension422;
    FelixClientThreadExtension423* extension423;
};
