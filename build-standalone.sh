#!/bin/sh
g++ -I. -Ifelix-interface src/felix_client_thread.cpp -ldl -lpthread -fpic -shared -o libfelix-client-thread.so

# and a test
g++ -I. -Ifelix-interface examples/mytest.cpp -L. -Wl,-rpath=. -lfelix-client-thread -fpic -o mytest
