#include <string>
#include <iostream>

#include "felix/felix_client_properties.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

void on_init() {
    printf("on_init called\n");
}

void on_connect(uint64_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void on_disconnect(uint64_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

int main(int argc, char** argv) {

  FelixClientThread::Config config;
  config.on_init_callback = on_init;
  config.on_connect_callback = on_connect;
  config.on_disconnect_callback = on_disconnect;

  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = std::string("lo");// "127.0.0.1"
  config.property[FELIX_CLIENT_LOG_LEVEL] = "trace";
  config.property[FELIX_CLIENT_BUS_DIR] = "./bus";
  config.property[FELIX_CLIENT_BUS_GROUP_NAME] = "FELIX";
  config.property[FELIX_CLIENT_VERBOSE_BUS] = "True";

  try {
    auto client = std::make_unique<FelixClientThread>(config);
  } catch (FelixClientException& e) {
    std::cout << e.what() << std::endl;
    return 1;
  } catch (std::runtime_error& rte) {
    std::cout << rte.what() << std::endl;
    return 2;
  }

  return 0;
}
